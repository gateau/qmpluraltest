# QMPluralTest

## Introduction

Simple program to test Qt-based translation support.

## Building the .po files

Generate the .pot following [these instructions][pot-instructions], but use
`l10n-kf5` instead of `l10n-kde4`.

[pot-instructions]: http://techbase.kde.org/Development/Tutorials/Localization/i18n_Build_Systems#Testing_your_Messages.sh_script

Generate a plural-only English .po with:

    lupdate -silent src/*.cpp -pluralonly -ts tmp.ts
    lconvert tmp.ts --sort-contexts --output-format po -target-language en -o po/qmpluraltest_qt-en.po

"Translate" the plural forms in `qmpluraltest_qt-en.po`.

Generate a "real" .po for your locale:

    msginit -i po/qmpluraltest_qt.pot -o po/qmpluraltest_qt-<your-language>.pot

Translate the generated .po

## Testing

Build *and install* qmpluraltest.

Run it: `qmpluraltest`.

Run it with the English locale: `LANG=en LANGUAGES=en qmpluraltest`.

Test the fallback to English locale:

- Remove the installed `qmpluraltest_qt-<your-language>.qm`

- Run `qmpluraltest`. Output should be the same as when it was run using the English locale.
